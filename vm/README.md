# Update Production from Local Machine

`DRUPALVM_ENV=prod ansible-playbook -i vm/inventory vendor/geerlingguy/drupal-vm/provisioning/playbook.yml -e "config_dir=$(pwd)/vm" --sudo --tags=drupal`

See https://www.jeffgeerling.com/blog/2017/soup-nuts-using-drupal-vm-build-local-and-prod
for more.

# Download Production Database and Restore to Local

`drush sql-sync --create-db @prod @local`
