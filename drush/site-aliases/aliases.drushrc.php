<?php
/**
 * @file
 * Drush Aliases for support.tkwd.co.
 */

$aliases['local'] = array(
  'root' => '/var/www/drupal/support/web',
  'uri' => 'http://vm.support.tkwd.co',
  // 'remote-host' => 'vm.support.tkwd.co',
  // 'remote-user' => 'vagrant',
  // 'ssh-options' => '-o PasswordAuthentication=no -i ' . drush_server_home() . '/.vagrant.d/insecure_private_key'
);

$aliases['prod'] = array(
  'root' => '/var/www/drupal/support/web',
  'uri' => 'http://support.tkwd.co',
  'remote-host' => 'support.tkwd.co',
  'remote-user' => 'tkwd',
);
